# *C*arrot *R*esult *Ag*gregator (crag)
Fetches aggregates the results from AWS's S3 bucket wcmc-data-stasis-result

## Files
- Launcher: cragLauncher.py
- Main module: crag/ folder

## Usage
